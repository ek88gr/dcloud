dcloud.service лежит в /etc/systemd/system

Запуск программы на prod-сервере:
1. Gradle -> distribution -> installBootDist
2. Переносим bin и lib из \build\install\dcloud-boot в \docker\volume\java
3. Файлы программы из папки \volume\java переносим на сервер в /usr/dcloud
4. В коммандной строке:
- sudo systemctl restart dcloud.service
- sudo systemctl status dcloud.service
