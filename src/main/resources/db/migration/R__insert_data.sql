drop table if exists log_element cascade;
create table log_element (id  bigserial not null, key varchar(255) not null, message varchar(255), path varchar(255) not null, remote_address varchar(255) not null, status int4 not null, unix_time int8 not null, user_agent varchar(255) not null, primary key (id));
