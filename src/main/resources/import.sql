-- 18074856, rqT2qtPRI8MUrQ
INSERT INTO "public"."partner_list_row" (i, email, status, owner_uid) VALUES (1, 'partner1@yandex.ru', 1, 18074856);
INSERT INTO "public"."yandex_key" (id, partner_id, key) VALUES (1, 1, 'rqT2qtPRI8MUrQ');

-- 1130000034674407, ZaQ1NbjlCC0W-w
-- https://disk.yandex.ru/d/ZaQ1NbjlCC0W-w/AIM-ONE/AC-320.jpg
INSERT INTO "public"."partner_list_row" (i, email, status, owner_uid) VALUES (2, 'partner2@yandex.ru', 0, 1130000034674407);
INSERT INTO "public"."yandex_key" (id, partner_id, key) VALUES (2, 2, 'rqT2qtPRI8MUrQ');

-- 454921432, XC2kld0GIwfi5A
-- https://disk.yandex.ru/d/XC2kld0GIwfi5A/Zabit/99524de4-093d-4b35-9116-f712ceae4c99.jpg
INSERT INTO "public"."partner_list_row" (i, email, status, owner_uid) VALUES (3, 'partner3@yandex.ru', 0, 111222333);
INSERT INTO "public"."yandex_key" (id, partner_id, key) VALUES (3, 3, 'XC2kld0GIwfi5A');

-- 237799829, 4BUWugdQwldI2g
INSERT INTO "public"."partner_list_row" (i, email, status, owner_uid) VALUES (4, 'partner4@yandex.ru', 1, 237799829);
INSERT INTO "public"."yandex_key" (id, partner_id, key) VALUES (4, 4, '4BUWugdQwldI2g');