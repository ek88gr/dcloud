package iisis.dcloud.sysLog;

import iisis.dcloud.sysLog.dao.SysLogRepository;
import iisis.dcloud.sysLog.dao.models.LogElement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class SysLogService {

    @Autowired
    SysLogRepository repository;

    public void save(String key, String path, String remoteAddress, String userAgent, Integer status, String message){
        final LogElement item = new LogElement();
        item.setKey(key);
        item.setPath(path);
        item.setRemoteAddress(remoteAddress);
        item.setUserAgent(userAgent);
        item.setStatus(status);
        item.setMessage(message);
        item.setUnixTime(new Date().getTime());

        repository.save(item);
    }

}
