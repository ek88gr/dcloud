package iisis.dcloud.sysLog.dao;

import iisis.dcloud.sysLog.dao.models.LogElement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SysLogRepository extends CrudRepository<LogElement, Long> {
}
