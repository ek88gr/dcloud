package iisis.dcloud.sysLog.dao.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "log_element")
public class LogElement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    String key;

    @Column(nullable = false)
    String path;

    @Column(nullable = false)
    Integer status;

    @Column
    String message;

    @Column(name = "unix_time", nullable = false)
    Long unixTime;

    @Column(nullable = false)
    String remoteAddress;

    @Column(nullable = false)
    String userAgent;
}