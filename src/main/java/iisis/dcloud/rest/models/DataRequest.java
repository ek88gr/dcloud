package iisis.dcloud.rest.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@AllArgsConstructor
public class DataRequest {
    private String key;
    private String path;
    private String remoteAddress;
    private String userAgent;
}
