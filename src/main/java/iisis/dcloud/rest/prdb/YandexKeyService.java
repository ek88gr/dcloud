package iisis.dcloud.rest.prdb;

import iisis.dcloud.rest.prdb.dao.YandexKeyRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Getter
@Setter
@Service
public class YandexKeyService {
    @Autowired
    YandexKeyRepository yandexKeyRepository;
}
