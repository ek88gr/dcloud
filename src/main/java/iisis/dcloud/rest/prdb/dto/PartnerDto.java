package iisis.dcloud.rest.prdb.dto;

import iisis.dcloud.rest.prdb.dao.model.PartnerListRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@AllArgsConstructor
public class PartnerDto {
    private Long i;
    private String email;
    private Integer status;
    private String ownerUid;

    public PartnerDto(PartnerListRow partnerListRow) {
        this.i = partnerListRow.getI();
        this.email = partnerListRow.getEmail();
        this.status = partnerListRow.getStatus();
        this.ownerUid = partnerListRow.getOwnerUid();
    }
}
