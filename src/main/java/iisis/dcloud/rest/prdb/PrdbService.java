package iisis.dcloud.rest.prdb;

import iisis.dcloud.rest.prdb.dao.PrdbRepository;
import iisis.dcloud.rest.prdb.dao.model.PartnerListRow;
import iisis.dcloud.rest.prdb.dto.PartnerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class PrdbService {

    @Autowired
    PrdbRepository prdbRepository;

    public PartnerDto getPartnerByOwnerUid(String ownerUid) {
        final PartnerListRow partnerListRow = prdbRepository.findPartnerListRowByOwnerUid(ownerUid);

        return Optional.ofNullable(partnerListRow)
                .map(v -> new PartnerDto(partnerListRow))
                .orElse(null);
    }


//    todo: пока не используем, возможно пригодится позднее
//    public PartnerListRow getPartnerListRowByYandexKey(String key) {
//        return prdbRepository.findPartnerListRowByYandexKey_Key(key);
//    }

//    todo: пока не используем, возможно пригодится позднее
//    public PartnerDto getPartnerDtoByYandexKey(String yandexKey) {
//        final var partnerListRow = this.getPartnerListRowByYandexKey(yandexKey);
//        return new PartnerDto(partnerListRow);
//    }

}
