package iisis.dcloud.rest.prdb;

import iisis.dcloud.rest.prdb.dto.PartnerDto;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Getter
@Setter
@Service
public class PartnerListModelCache {

    private final ConcurrentHashMap<String, PartnerDto> yandexKeyPartnerDtoMap = new ConcurrentHashMap<>();

    public void savePartner(String key, PartnerDto partner) {
        yandexKeyPartnerDtoMap.put(key, partner);
    }

    public PartnerDto getPartnerByYandexKey(String yandexKey) {
        return yandexKeyPartnerDtoMap.get(yandexKey);
    }

}