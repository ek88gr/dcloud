package iisis.dcloud.rest.prdb.dao.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "partner_list_row", indexes = {@Index(name = "index_uid_partner_list_row", columnList = "owner_uid", unique = true)})


public class PartnerListRow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long i;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private Integer status;

    @Column(nullable = false, name = "owner_uid")
    private String ownerUid;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "partnerListRow")
    private List<YandexKey> yandexKey;

}
