package iisis.dcloud.rest.prdb.dao;

import iisis.dcloud.rest.prdb.dao.model.YandexKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface YandexKeyRepository extends CrudRepository<YandexKey, Long> {
}
