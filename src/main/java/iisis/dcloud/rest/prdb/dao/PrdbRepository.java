package iisis.dcloud.rest.prdb.dao;

import iisis.dcloud.rest.prdb.dao.model.PartnerListRow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrdbRepository extends CrudRepository<PartnerListRow, Long> {
    PartnerListRow findPartnerListRowByOwnerUid(String ownerUid);
    PartnerListRow findPartnerListRowByYandexKey_Key(String yandexKey);
}