package iisis.dcloud.rest;

import com.squareup.okhttp.Response;
import iisis.dcloud.rest.exceptions.UniversalException;
import iisis.dcloud.rest.models.DataRequest;
import iisis.dcloud.yandexDisk.YandexDiskService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@Slf4j
@RestController
public class ResourcesRestController {

    @Autowired
    YandexDiskService service;

    @GetMapping(value = "/d/{key}/**")
    public ResponseEntity<InputStreamResource> getURLValue(HttpServletRequest request, @PathVariable("key") String key) throws UniversalException {

        String remoteAddress = request.getHeader("X-Real-IP");
        if (remoteAddress == null) {
            log.warn("request.getHeader(\"X-Real-IP\") == null. Nginx doesn't proxy header X-Real-IP");
            remoteAddress = "0.0.0.0";
        }
        String userAgent = request.getHeader("User-Agent");

        log.info("URL: {}", "https://yadi.sk" + request.getRequestURI());

        final String uri = request.getRequestURI();
        final String path;

        try {
            path = URLDecoder.decode(uri.substring(uri.lastIndexOf(key) + key.length()), StandardCharsets.UTF_8.toString());

        } catch (Exception e) {
            throw new UniversalException(key, "", remoteAddress, userAgent, HttpStatus.BAD_REQUEST,
                    "Incorrect folder identifier in the Yandex service or it is missing. More about the error here: https://");
        }

        final DataRequest dataRequest = new DataRequest(key, path, remoteAddress, userAgent);

        return service.getPublicResourceV4(dataRequest);

    }

    @RequestMapping(value = "/**")
    public Response getBadRequestError(HttpServletRequest request) {
        String remoteAddress = request.getHeader("X-Real-IP");
        if (remoteAddress == null) {
            log.warn("request.getHeader(\"X-Real-IP\") == null. Nginx doesn't proxy header X-Real-IP");
            remoteAddress = "0.0.0.0";
        }
        String userAgent = request.getHeader("User-Agent");
//        Test: "Bad Request" - OK
        throw new UniversalException("", "", remoteAddress, userAgent, HttpStatus.BAD_REQUEST, "Bad Request");
    }

}
