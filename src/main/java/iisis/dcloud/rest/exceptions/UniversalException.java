package iisis.dcloud.rest.exceptions;

import iisis.dcloud.rest.models.DataRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Getter
@Setter
public class UniversalException extends ResponseStatusException { //Универсальное исключение

    private String key;
    private String path;
    private String remoteAddress;
    private String userAgent;
    private HttpStatus httpStatus;
    private String message;

    public UniversalException(DataRequest dataRequest, HttpStatus httpStatus, String message) {
        super(httpStatus);
        this.key = dataRequest.getKey();
        this.path = dataRequest.getPath();
        this.remoteAddress = dataRequest.getRemoteAddress();
        this.userAgent = dataRequest.getUserAgent();
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public UniversalException(String key, String path, String remoteAddress, String userAgent, HttpStatus httpStatus, String message) {
        super(httpStatus);
        this.key = key;
        this.path = path;
        this.remoteAddress = remoteAddress;
        this.userAgent = userAgent;
        this.httpStatus = httpStatus;
        this.message = message;

    }

}
