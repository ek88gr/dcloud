package iisis.dcloud.rest.exceptions;

import iisis.dcloud.sysLog.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler { //Обработчик исключений

    @Autowired
    SysLogService sysLogService;

    @ExceptionHandler(UniversalException.class)
    public ResponseEntity<Object> handleException(UniversalException e) {

        try {
            sysLogService.save(e.getKey(), e.getPath(), e.getRemoteAddress(), e.getUserAgent(), e.getHttpStatus().value(), e.getMessage());
        } catch (Exception ex) {
            log.error("Error: {}", ex.getMessage());
        }

        return buildResponseEntity(new ApiError(e.getHttpStatus(), e.getMessage(), e.getPath()));

    }

    public ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

}