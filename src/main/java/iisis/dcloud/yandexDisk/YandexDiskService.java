package iisis.dcloud.yandexDisk;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.OkHttpClientFactory;
import com.yandex.disk.rest.json.Link;
import com.yandex.disk.rest.retrofit.CloudApi;
import com.yandex.disk.rest.retrofit.ErrorHandlerImpl;
import com.yandex.disk.rest.retrofit.RequestInterceptorImpl;
import iisis.dcloud.rest.exceptions.UniversalException;
import iisis.dcloud.rest.models.DataRequest;
import iisis.dcloud.rest.prdb.PartnerListModelCache;
import iisis.dcloud.rest.prdb.PrdbService;
import iisis.dcloud.rest.prdb.dto.PartnerDto;
import iisis.dcloud.sysLog.SysLogService;
import iisis.dcloud.yandexDisk.apiClient.RestClientIO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

import java.io.InputStream;

@Slf4j
@Getter
@Setter
@Service
public class YandexDiskService {

    private final String serverUrl = "https://cloud-api.yandex.net";
    private final Credentials credentials = new Credentials("", "");
    private final OkHttpClient client = OkHttpClientFactory.makeClient();

    @Autowired
    SysLogService sysLogService;

    @Autowired
    PrdbService prdbService;

    @Autowired
    PartnerListModelCache partnerListModelCache;

//    1. проверка доступности на запрос к диску
//    2. получение ссылки
    public String getPublicResourceDownloadLink(DataRequest dataRequest) throws UniversalException {

        final PartnerDto partnerDto = partnerListModelCache.getPartnerByYandexKey(dataRequest.getKey());

        if (partnerDto != null) {
//            restricted access status
            if (partnerDto.getStatus().equals(0)) {
//                1й запрос, не проинициализирована хэш таблица или не оплачен доступ
//                Test: "Pay for access." - OK
                throw new UniversalException(dataRequest, HttpStatus.FORBIDDEN, "Subscription expired, pay for access.");
            }
        }

        final String publicKey = "https://yadi.sk/d/" + dataRequest.getKey();

        CloudApi cloudApi = new RestAdapter.Builder()
                .setClient(new OkClient(client))
                .setEndpoint(serverUrl)
                .setRequestInterceptor(new RequestInterceptorImpl(credentials.getHeaders()))
                .setErrorHandler(new ErrorHandlerImpl())
                .build()
                .create(CloudApi.class);

        final Link link;
        final MultiValueMap<String, String> parameters;
        try {
            link = cloudApi.getPublicResourceDownloadLink(publicKey, dataRequest.getPath());

            parameters = UriComponentsBuilder.fromUriString(link.getHref()).build().getQueryParams();

        } catch (Exception e) {
//            Test: "Internal Server Error" - OK
            throw new UniversalException(dataRequest, HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
        }

        final String uid = parameters.getFirst("uid");
        final String owner_uid = parameters.getFirst("owner_uid");

        final PartnerDto partner = prdbService.getPartnerByOwnerUid(owner_uid); // partnerDtoFromDataBase

        if (partner == null) {
            log.warn("Partner: key - {}, path - {}, IP - {}, userAgent - {} not found in database",
                    dataRequest.getKey(), dataRequest.getPath(), dataRequest.getRemoteAddress(), dataRequest.getUserAgent());
            throw new UniversalException(dataRequest, HttpStatus.UNAUTHORIZED,
                    "You need to register at: https://fapi.iisis.ru/hosting-kartinok-s-pryamoj-ssylkoj");

        } else {
            partnerListModelCache.savePartner(dataRequest.getKey(), partner);

            if (partner.getStatus().equals(0)) {
//                1й запрос, не проинициализирована хэш таблица или не оплачен доступ
//                Test: "Subscription expired, pay for access." - OK
                throw new UniversalException(dataRequest, HttpStatus.FORBIDDEN, "Subscription expired, pay for access.");
            }
        }

        if (!parameters.containsKey("fsize")) {
            log.error("path is directory. key {}, path {}, uid {}, owner_uid {}", dataRequest.getKey(), dataRequest.getPath(), uid, owner_uid);
            throw new UniversalException(dataRequest, HttpStatus.INTERNAL_SERVER_ERROR, "path is directory");
        }

//        Test: "File size > 15_000_000L" - OK
        if (Long.parseLong(parameters.getFirst("fsize")) > 15_000_000L) {
            log.error("File size > 15_000_000L. key {}, path {}, uid {}, owner_uid {}", dataRequest.getKey(), dataRequest.getPath(), uid, owner_uid);
            throw new UniversalException(dataRequest, HttpStatus.INTERNAL_SERVER_ERROR, "File size > 15_000_000L");
        }

        return link.getHref();
    }

    public ResponseEntity<InputStreamResource> getPublicResourceV4(DataRequest dataRequest) throws UniversalException {

        final String linkHref = this.getPublicResourceDownloadLink(dataRequest);

        ResponseBody responseBody;
        try {
            responseBody = new RestClientIO(client, credentials.getHeaders()).downloadUrl(linkHref);
        } catch (Exception e) {
            throw new UniversalException(dataRequest, HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }

        InputStream is;
        try {
            is = responseBody.byteStream();

        } catch (Exception e) {
            throw new UniversalException(dataRequest, HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }

        final MediaType mediaType;
        try {
            com.squareup.okhttp.MediaType t = responseBody.contentType();
            mediaType = MediaType.parseMediaType(t.toString());

        } catch (Exception e) {
            throw new UniversalException(dataRequest, HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }

        sysLogService.save(dataRequest.getKey(), dataRequest.getPath(), dataRequest.getRemoteAddress(),
                dataRequest.getUserAgent(), HttpStatus.OK.value(), null);
        return ResponseEntity.ok()
                .contentType(mediaType)
                .body(new InputStreamResource(is));

    }

}